<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'historique_spip32_h3_description' => 'Plugin de compatibilité pour conserver des intertitres H3',
	'historique_spip32_h3_nom' => 'Intertitres H3',
	'historique_spip32_h3_slogan' => '',
);
