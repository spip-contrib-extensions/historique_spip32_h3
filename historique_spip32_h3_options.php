<?php
/**
 * Options au chargement du plugin Intertitres H3
 *
 * @plugin     Intertitres H3
 * @copyright  2021
 * @author     spip team
 * @licence    GNU/GPL
 * @package    SPIP\Historique_spip32_h3\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!isset($GLOBALS['debut_intertitre'])) {
	$GLOBALS['debut_intertitre'] = '<h3 class="spip">';
}
if (!isset($GLOBALS['fin_intertitre'])) {
	$GLOBALS['fin_intertitre'] = '</h3>';
}

